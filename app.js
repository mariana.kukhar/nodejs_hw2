const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();
const port = process.env.PORT || 8080;

const authRouter = require('./routers/authRouter');
const noteRouter = require('./routers/noteRouter');
const userRouter = require('./routers/userRouter');

app.use(express.json());
app.use(express.static('build'));
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/notes', noteRouter);
app.use('/api/users/me', userRouter);
app.use((err, req, res, next) => {
  res.status(500).json({ message: err.message });
});

const start = async () => {
  try {
    await mongoose.connect(
      'mongodb+srv://taskapp:taskapp@cluster0.sd3er.mongodb.net/task-api?retryWrites=true&w=majority',
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true
      }
    );
  } catch (e) {
    console.log(e.message);
  }

  app.listen(port, () => {
    console.log(`Server works at port ${port}!`);
  });
};

start();
