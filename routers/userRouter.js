const express = require('express');
const router = express.Router();
const auth = require('../middlewares/auth');
const bcrypt = require('bcrypt');

router.get('/', auth, async (req, res) => {
  if (req.user) {
    res.status(200).json({ user: req.user });
  } else {
    res.status(400).json({ message: 'Please autheticate!' });
  }
});

router.delete('/', auth, async (req, res) => {
  try {
    await req.user.remove();
    res.status(200).json({ message: 'Success' });
  } catch (e) {
    res.status(400).json({ message: e.message });
  }
});

router.patch('/', auth, async (req, res) => {
  const user = req.user;
  const { oldPassword, newPassword } = req.body;
  if (oldPassword && newPassword) {
    if (!(await bcrypt.compare(oldPassword, user.password))) {
      return res.status(400).json({ message: `Wrong password!` });
    } else {
      if (newPassword != oldPassword) {
        user.password = await bcrypt.hash(newPassword, 10);
        await user.save();
        res.status(200).json({ message: 'Success' });
      }
    }
  } else {
    res.status(400).json({ message: 'Enter all needed credentials!' });
  }
});

module.exports = router;
