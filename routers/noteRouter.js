const express = require('express');
const router = express.Router();
const auth = require('../middlewares/auth');
const { Note } = require('../models/noteModel');

router.post('/', auth, async (req, res) => {
  const note = new Note({
    userId: req.user._id,
    text: req.body.text
  });
  try {
    await note.save();
    res.status(200).json({ message: 'Success' });
  } catch (e) {
    res.status(400).json({ message: e.message });
  }
});

router.get('/', auth, async (req, res) => {
  try {
    await req.user
      .populate({
        path: 'notes',
        options: {
          limit: parseInt(req.query.limit),
          skip: parseInt(req.query.offset)
        }
      })
      .execPopulate();
    res.status(200).json({ notes: [...req.user.notes] });
  } catch (e) {
    res.status(400).json({ message: e.message });
  }
});

router.get('/:id', auth, async (req, res) => {
  let _id = req.params.id;

  const note = await Note.findOne({ _id, userId: req.user._id });

  if (!note) {
    res.status(400).json({ message: 'No such task' });
  }
  res.status(200).send(note);
});

router.put('/:id', auth, async (req, res) => {
  let _id = req.params.id;
  const note = await Note.findOne({ _id, userId: req.user._id });
  if (!note) {
    res.status(400).json({ message: 'No such task' });
  }
  if (req.body.text) {
    if (note.text === req.body.text) {
      res.status(400).json({ message: 'Enter new description of your note' });
    } else {
      note.text = req.body.text;
      await note.save();
      res.status(200).json({ message: 'Success' });
    }
  } else {
    res.status(400).json({ message: 'Enter text field' });
  }
});

router.patch('/:id', auth, async (req, res) => {
  let _id = req.params.id;
  const note = await Note.findOne({ _id, userId: req.user._id });
  if (!note) {
    res.status(400).json({ message: 'No such task' });
  }
  note.completed = !note.completed;
  await note.save();
  res.status(200).json({ message: 'Success' });
});

router.delete('/:id', auth, async (req, res) => {
  let _id = req.params.id;
  const note = await Note.findOne({ _id, userId: req.user._id });
  if (!note) {
    res.status(400).json({ message: 'No such task' });
  }
  await Note.findOneAndDelete({
    _id,
    userId: req.user._id
  });
  res.status(200).json({ message: 'Success' });
});

module.exports = router;
