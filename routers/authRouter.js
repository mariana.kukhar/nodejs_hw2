const express = require('express');
const router = express.Router();
const { asyncWrapper } = require('../helpers/helpers');
const {
  validateRegistration
} = require('../registration/validateRegistration');
const { registration, login } = require('../registration/registartion');



router.post(
  '/register',
  asyncWrapper(validateRegistration),
  asyncWrapper(registration)
);
router.post('/login', asyncWrapper(login));

module.exports = router;
