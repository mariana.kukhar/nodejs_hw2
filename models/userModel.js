const { response } = require('express');
const mongoose = require('mongoose');
const { Note } = require('./noteModel');

const userSchema = mongoose.Schema(
  {
    username: {
      type: String,
      required: true,
      uniq: true
    },
    password: {
      type: String,
      required: true
    },
    createdDate: {
      type: Date,
      default: Date.now()
    },
    tokens: [
      {
        token: {
          type: String
        }
      }
    ]
  },
  {
    collection: 'users'
  }
);

userSchema.virtual('notes', {
  ref: 'Note',
  localField: '_id',
  foreignField: 'userId'
});

userSchema.methods.toJSON = function () {
  const user = this;
  const userObject = user.toObject();

  delete userObject.password;
  delete userObject.tokens;
  delete userObject.__v;
  userObject._id.toString();

  return userObject;
};

userSchema.pre('remove', async function (next) {
  const user = this;
  await Note.deleteMany({ userId: user.id });
  next();
});

module.exports.User = mongoose.model('User', userSchema);
