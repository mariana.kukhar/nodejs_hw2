const mongoose = require('mongoose');

const noteSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'User'
    },
    completed: {
      type: Boolean,
      default: false
    },
    text: {
      type: String,
      required: true,
      trim: true
    },
    createdDate: {
      type: Date,
      default: Date.now()
    }
  },
  {
    collection: 'notes'
  }
);

noteSchema.methods.toJSON = function () {
  const note = this;
  const noteObject = note.toObject();

  delete noteObject.__v;
  noteObject.userId.toString();

  return noteObject;
};

module.exports.Note = mongoose.model('Note', noteSchema);
