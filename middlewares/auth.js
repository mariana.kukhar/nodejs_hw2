const jwt = require('jsonwebtoken');
const { User } = require('../models/userModel');
const { JWT_SECRET } = require('../config');

const auth = async (req, res, next) => {
  const token = req.headers['authorization'];

  if (!token) {
    return res
      .status(400)
      .json({ message: `No Authorization http header found!` });
  }

  const decoded = jwt.verify(token, JWT_SECRET);

  const user = await User.findOne({
    _id: decoded._id,
    'tokens.token': token
  });

  if (!user) {
    res.status(400).json({ message: 'Please authenticate!' });
  }
  req.token = token;
  req.user = user;
  next();
};

module.exports = auth;
